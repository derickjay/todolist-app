

import ToDoList from "./pages/ToDoList"
import {Container} from "react-bootstrap"

import './App.css';


function App() {
  return (
    <>
      <Container>
      < ToDoList/>
      </Container>
    </>
  );
}

export default App;
