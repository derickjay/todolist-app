import React, { useState, useEffect } from "react";
import { Container, ListGroup, FormControl, Button } from 'react-bootstrap';


function ToDoList() {
  const [toDoLists, setToDoLists] = useState([]);
  const [newTitle, setNewTitle] = useState("");
  const [editId, setEditId] = useState("");
  const [editTitle, setEditTitle] = useState("");
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/todos`)
      .then((response) => response.json())
      .then((data) => setToDoLists(data));
  }, [refresh]);

  const handleCreate = () => {
    fetch(`${process.env.REACT_APP_API_URL}/todos/addToDoList`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ title: newTitle }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data && data.inputError) {
          alert("Please enter a title");
        } else {
          setToDoLists([...toDoLists, data]);
          setNewTitle("");
          setRefresh(!refresh)
        }
      });
  };

  const handleDelete = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}/todos/${id}/delete`, {
      method: "DELETE",
    })
      .then((response) => response.json())
      .then((data) => {
        if (data && data.notFoundError) {
          alert("To-do list not found");
        } else {
          const newList = toDoLists.filter((list) => list._id !== id);
          setToDoLists(newList);
        }
      });
  };

  const handleEdit = (id) => {
    const list = toDoLists.find((list) => list._id === id);
    setEditId(id);
    setEditTitle(list.title);
  };

  const handleUpdate = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}/todos/${id}/updateToDoList`, {
      method: "PATCH",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ title: editTitle }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data && data.notFoundError) {
          alert("To-do list not found");
        } else {
          const newList = toDoLists.map((list) =>
            list._id === id ? { ...list, title: editTitle } : list
          );
          setToDoLists(newList);
          setEditId("");
          setEditTitle("");
        }
      });
  };

  return (
    <Container>
      <div>
        <h1 className="text-center mt-md-5 mb-md-4">To-Do List</h1>
        <div className="d-flex justify-content-center">
          <FormControl
            className="w-50"
            type="text"
            value={newTitle}
            onChange={(e) => setNewTitle(e.target.value)}
          />
          <Button variant="primary" onClick={handleCreate}>Add</Button>
        </div>
        <div className="d-flex justify-content-center mt-md-5">
          <ListGroup className="w-50">
            {toDoLists.map((list) => (
              <ListGroup.Item key={list._id}>
                {editId === list._id ? (
                  <div>
                    <FormControl
                      type="text"
                      value={editTitle}
                      onChange={(e) => setEditTitle(e.target.value)}
                    />
                    <Button variant="primary" onClick={() => handleUpdate(list._id)}>Save</Button>
                  </div>
                ) : (
                  <div className="d-flex">
                    <div>
                    <span id="list_title">{list.title}</span>
                    </div>
                    <div className="ms-md-auto">
                    <Button variant="secondary" onClick={() => handleEdit(list._id)}>Edit</Button>
                    <Button variant="danger" onClick={() => handleDelete(list._id)}>Delete</Button>
                    </div>
                  </div>
                )}
              </ListGroup.Item>
            ))}
          </ListGroup>
        </div>
      </div>
    </Container>
  );
  
}

export default ToDoList;
